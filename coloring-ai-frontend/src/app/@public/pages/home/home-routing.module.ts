import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Si no importa el componente hay que hacerlo
import { HomeComponent } from './home.component';

const routes: Routes = [
  // Se agrega la ruta path vacia por que es el ppal
  // y el componente
  {

    path: '',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
