Proyecto de creación de página web para cumplir los requisitos del curso DESARROLLO DE APLICACIONES WEB: 2021-4-1711-B6-Presencial dado por la UIS para el programa Misión TIC 2022.

El proyecto consiste en implementar y calibrar un algoritmo en Tensor Flow de visión por computador ([basado en este enlace](https://becominghuman.ai/auto-colorization-of-black-and-white-images-using-machine-learning-auto-encoders-technique-a213b47f7339)) que coloree imágenes en blanco y negro.

Una vez se haya implementado el algoritmo en python, se debe llevar a tensorflow.js para embeber el código en una página web desarrollada en js.
