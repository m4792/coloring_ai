// Importar conexión a MongoDB
const mongoose = require("mongoose");

//Importar dependencias de MongoDB
// para salir de la carpeta y que el servidor encuentre el index agregar ../ antes del archivo
// Lo anterior pasa por qué db.js esta en config, e index esta una carpeta por arriba.
// El ../ se repite según el número de carpetas que este anidado el programa de ejecución db.js p.e
// se equivocó, volvió a la inicial de var.env, ahí si funciona

require('dotenv').config({path: 'var.env'})

// Se hace una función asincrónica, es decir, que espera a que pase algo
// antes de ejcutar otra cosa.  Se hace flecha, por que no requiere variables.
// Como es una sola base de datos, se hace una entrada constante y no variable.

const conexionDB = async() =>{
    // Try catch en js parecido a python, con cath en lugar del except
    try {
        await mongoose.connect(process.env.URL_MONGODB);
        console.log("conexión establecida con MONGODB Atlas");
    } catch(error){
        console.log("Error de Conexión a la Base de Datos'");
        console.log(error);
        process.exit(1);
    }
}

// Para que pueda leerlo el index.js
module.exports = conexionDB;