// importar módulo de MongoDB =>  mongoose
const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
    id:Number,
    nombre:String,
    apellido:String,
    email:String,
    psw:String
    },
    {
        versionKey: false,
        timestamps: true
    }
);

// Para poderlo usar en los demas archivos hay que exportarlo

module.exports = mongoose.model('users', userSchema);